<?php
function autoload( $classNameWithNamespace ) {
	$ds = DIRECTORY_SEPARATOR;
	$parts = explode( '\\', $classNameWithNamespace );
	$className = array_pop($parts);

	if ( ! empty( $parts[0] ) && $parts[0] === 'Vodx') {
		array_shift( $parts );
		$expectedPath = dirname(__FILE__) . '/cls/' . implode( $ds, $parts );
		$expectedName = "$expectedPath/$className.php";

		if ( file_exists( $expectedName ) ) {
			require_once $expectedName;
		}
	}
}

spl_autoload_register('autoload');