(function ($) {
  $(document).ready(function() {
    var wrapper = $('.news-list');

    if ( wrapper.length ) {
      getFromFeed();
    }
  });

  function getFromFeed() {
    $.ajax({
      url: 'actions/get_news.php',
      dataType: 'JSON',
      method: 'GET',
      data: {},
      complete: function (response) {
      },
      success: function (response) {
        if (response != undefined && response.length) {
          response.forEach(function(item, k) {
            $('.js-news-slider').slick('slickAdd', item);
          });
        }
      },
      beforeSend: function () {
      },
      error: function (evt) {
      }
    });
  }

})(jQuery);