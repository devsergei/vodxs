<?php
namespace Vodx;
require_once '../including_external_files.php';

$news = new news();
$articles = $news->get_feed_news_json(15);

if ( ! empty( $articles ) ) {
  echo $articles;
}