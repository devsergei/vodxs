<?php
namespace Vodx;
require_once 'including_external_files.php';

$routes = new routes( $_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI'] );
$page_name = $routes->get_page();
$vars = $routes->get_vars();

if ( ! empty( $page_name ) ) {
	include $page_name;
} else {
	include "main.php";
}
?>