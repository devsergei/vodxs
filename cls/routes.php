<?php
namespace Vodx;
/**
 * routes
 *
 */
class routes {

	private $dispatcher;
	private $uri;
	private $httpMethod;
	private $page;
	private $vars;

	public function __construct( $httpMethod, $uri ) {
		$this->httpMethod = $httpMethod;
		$this->uri = $uri;
		$this->get_dispatcher();
		$this->strip_query();
		$this->route_processing();
	}

	public function get_page() {
		return $this->page;
	}

	public function get_vars() {
		return $this->vars;
	}

	private function get_dispatcher() {
		$this->dispatcher = \FastRoute\simpleDispatcher(function(\FastRoute\RouteCollector $r) {
			$r->addRoute('GET', '/', 'main.php');
			$r->addRoute('GET', '/news-single[/[{post}[/[{slug}[/]]]]]', 'news-single.php');
			$r->addRoute('GET', '/news[/[{page}[/[{date}[/]]]]]', 'news-page.php');
			$r->addRoute('GET', '/sponsor[/]', 'sponsor.php');
			$r->addRoute('GET', '/wp[/]', 'wp.php');

			// old urls
			$r->addRoute('GET', '/news-page.html', 'news-page.php');
			$r->addRoute('GET', '/news-single.html?post={post}', 'news-single.php');
			$r->addRoute('GET', '/sponsor.html', 'sponsor.php');
			$r->addRoute('GET', '/wp/index.html', 'wp.php');
			
			// $r->addRoute('POST', '/ajax[/[{action}[/]]]', 'ajax');
		});
	}

	private function strip_query() {
		// Strip query string (?foo=bar) and decode URI
		if ( false !== $pos = strpos( $this->uri, '?' ) ) {
			$uri = substr( $this->uri, 0, $pos );
		}

		$this->uri = rawurldecode( $this->uri );
	}

	private function route_processing() {
		$routeInfo = $this->dispatcher->dispatch( $this->httpMethod, $this->uri );
		switch ( $routeInfo[0] ) {
			case \FastRoute\Dispatcher::NOT_FOUND:
				$this->page = '404.php';
				$this->vars = [];
				break;
			case \FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
				$allowedMethods = $routeInfo[1];
				$this->page = '404.php';
				$this->vars = [];
				break;
			case \FastRoute\Dispatcher::FOUND:
				$this->page = $routeInfo[1];
				$this->vars = [];
				foreach ( $routeInfo[2] as $v_key => $var ) {
					$this->vars[ $v_key ] = str_replace( '/', '', $var );
				}
				break;
		}
	}

}
