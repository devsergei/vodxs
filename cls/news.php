<?php
namespace Vodx;
/**
 * news
 *
 */
class news {
  public function get_feed_news( $page = 0, $date = '', $limit = 5 ) {
    $ctx = stream_context_create(array('http' => array('timeout' => 10)));
    $articles = '';
    $articles_arr = [];

    $url = 'https://feeds.blogcertified.com/posts?categories=1122&num_of_posts=' . $limit;

    if ( ! empty( $date ) ) {
      $url .= '&type=ordered_by_year&year=' . $date;
    } else {
      $url .= '&type=order_desc';
    }

    if ( ! empty( $page ) && $page > 0 ) {
      $url .= '&page=' . $page;
    }

    $articles = @file_get_contents( $url, 0, $ctx );

    if ( ! empty( $articles ) ) {
      $articles = json_decode( $articles );
      if ( isset( $articles->next_page ) ) {
        $articles->prev_page = ($articles->next_page - 2) >= 1 ? ($articles->next_page - 2) : 0;
        if ( $articles->current_posts_count == 0 ) {
          $articles->next_page = $articles->next_page -1;
        }
      }
    
      if ( ! empty( $articles->data ) ) {
        foreach ( $articles->data as $ar_k => $ar_v ) {
          $articles_arr[ $ar_k ] = $this->getPost( $ar_v );
          $articles_arr[ $ar_k ]->share = $this->generateShareLinks( $articles_arr[ $ar_k ]->share );
        }
        $articles->data = $articles_arr;
      }

      $articles->page_prev_link = '/news/' . $articles->prev_page . '/';
      $articles->page_prev_link .= ! empty( $date ) ? $date . '/' : '';

      $articles->next_page_link = '/news/' . $articles->next_page . '/';
      $articles->next_page_link .= ! empty( $date ) ? $date . '/' : '';
    }
    
    return $articles;
  }

  public function get_feed_single_news( $post_id ) {
    $ctx = stream_context_create(array('http' => array('timeout' => 10)));
    $article = '';
    $article_latest = '';
    $article = @file_get_contents('https://feeds.blogcertified.com/posts/' . $post_id, 0, $ctx);
    $article_latest = @file_get_contents('https://feeds.blogcertified.com/posts?type=order_desc&categories=1122&num_of_posts=5', 0, $ctx);

    if ( ! empty( $article ) ) {
      $article = json_decode( $article );
    
      if ( ! empty( $article->data ) ) {
        $article = $this->getPost( $article->data );
        $article->share_links = $this->generateShareLinks( $article->share );
      }
    }

    if ( ! empty( $article_latest ) ) {
      $article_latest = json_decode( $article_latest );
    
      if ( ! empty( $article_latest->data ) ) {
        $article_latest_arr = [];
        $counter = 0;
        foreach ( $article_latest->data as $al_k => $al_v ) {
          if ( $al_v->id != $article->id && $counter < 4 ) {
            $article_latest_arr[ $al_k ] = $this->getPost( $al_v );
            $article_latest_arr[ $al_k ]->share = $this->generateShareLinks( $article_latest_arr[ $al_k ]->share );
            $counter++;
          }
        }

        if ( ! empty( $article ) ) {
          $article->latest_news = $article_latest_arr;
        }
      }
    }
    return $article;
  }

  public function get_feed_news_json( $limit = 5 ) {
    $articles = $this->get_feed_news( 1, '', $limit );
    $articles_arr = [];

    if ( ! empty( $articles->data ) ) {
      foreach ( $articles->data as $a_key => $article ) {
        $post_link = '/news-single/' . $article->id . '/' . $article->slug . '/';
        $post_date = preg_split("/[\s,]+/", $article->date);
        $post_date = isset( $post_date[0] ) ? $post_date[0] . ' ' . $post_date[1] : '';
        $image_type = '360x200';

        $item = '<div class="news-list__item">';
          $item .= '<div class="news-list__item-wrapper">';
            $item .= '<div class="news-list__item-date">' . $post_date . '</div>';
            $item .= '<div class="news-list__item-image">';
              $item .= '<a href="'. $post_link .'">';
                $item .= '<img src="'. $article->cached_thumbs->$image_type .'" alt="'. $article->title .'">';
              $item .= '</a>';
            $item .= '</div>';
            $item .= '<div class="news-list__item-txt">';
              $item .= '<a class="news-list__item-name" href="'. $post_link .'">'. $article->title .'</a>';
              $item .= '<p class="news-list__item-text">'. $this->get_short_content( $article->description ) .'</p>';
            $item .= '</div>';
            $item .= '<a class="news-list__item-more" href="'. $post_link .'">read more</a>';
          $item .= '</div>';
        $item .= '</div>';
        $articles_arr[] = $item;
      }
    }

    return json_encode( $articles_arr );
  }

  public function get_short_content( $content ) {
    $max_length = 250; // maximum number of characters to extra

    if ( strlen( $content ) <= $max_length) {
      return $content;
    }

    $trimmed_string = substr( $content, 0, $max_length );

    $trimmed_string = substr( $trimmed_string, 0, min( strlen( $trimmed_string ), strrpos( $trimmed_string, " " )));
    $trimmed_string = $trimmed_string . '...';
    return $trimmed_string;
  }

  public function getPost( $article ) {
    $pattern = '/^([ |\t]*?)([^ |\t].*?)([ |\t]*?)$/m';
    $article->content = preg_replace( $pattern, "<p>$2</p>", $article->content );
    $date = new \DateTime( $article->date );
    $article->date = $date->format('F d');
    $article->slug = $this->slugify( $article->title );
    $article->url = "https://" . $_SERVER['SERVER_NAME'] . '/news-single/' . $article->id . '/' . $article->slug . '/';
    $article->share = new \stdClass();
    $article->share->title = urlencode( $article->title );
    $article->share->description = urlencode( $article->description );
    $article->share->image = urlencode( $article->image );
    $article->share->url = urlencode( $article->url );
  
    return $article;
  }

  public function generateShareLinks( $share ) {
    $links['facebook'] = [
      'link'  => 'https://www.facebook.com/sharer.php?s=100&p[title]='. $share->title .'&u='. $share->url
              .'&t='. $share->title .'&p[summary]='. $share->description .'&p[url]='. $share->url,
      'class' => 'fb',
    ];
  
    $links['plus'] = [
      'link'  => 'https://plus.google.com/share?url='. $share->url,
      'class' => 'gg',
    ];
  
    $links['twitter'] = [
      'link'  => 'https://twitter.com/intent/tweet?url='. $share->url .'&text='. $share->description,
      'class' => 'tw',
    ];
  
    $links['pinterest'] = [
      'link'  => 'https://www.pinterest.com/pin/create/button/?media='. $share->image .'&url='. $share->url
              .'&description='. $share->description,
      'class' => 'pp',
    ];
  
    $links['blogger'] = [
      'link'  => 'https://blogger.com/blog-this.g?t='. $share->description .'&n='. $share->title .'&u='. $share->url,
      'class' => 'bb'
    ];
  
    $links['linkedin'] = [
      'link'  => 'https://www.linkedin.com/shareArticle?mini=true&url='. $share->url .'&title='
              . $share->title .'&summary='. $share->description .'&source='. $share->image,
      'class' => 'in',
    ];
  
    $links['reddit'] = [
      'link'  => 'http://reddit.com/submit?url='. $share->url .'&title='. $share->title,
      'class' => 'rd',
    ];
  
    $result = '';
    foreach ( $links as $link_k => $link ) {
      $result .= '<div class="shr-box-dropdown-item">';
        $result .= '<a class="shr-box-item '. $link["class"] .'" href="'. $link["link"] .'" target="_blank"></a>';
      $result .= '</div>';
    }
  
    return $result;
  }

  public function slugify( $text ) {
		// replace non letter or digits by -
		$text = preg_replace('~[^\pL\d]+~u', '-', $text);

		// transliterate
		$text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);

		// remove unwanted characters
		$text = preg_replace('~[^-\w]+~', '', $text);

		// trim
		$text = trim($text, '-');

		// remove duplicate -
		$text = preg_replace('~-+~', '-', $text);

		// lowercase
		$text = strtolower($text);

		if (empty($text)) {
		  return 'n-a';
		}

		return $text;
	}
}
